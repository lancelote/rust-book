fn main() {
    let v1 = vec![1, 2, 3];

    let mut v1_iter = v1.iter();

    // We get immutable references to the values in the vector calling `next`
    assert_eq!(v1_iter.next(), Some(&1));
    assert_eq!(v1_iter.next(), Some(&2));
    assert_eq!(v1_iter.next(), Some(&3));
    assert_eq!(v1_iter.next(), None);

    // Use `into_iter` to force iterator to take ownership over `v1` and return owned items
    // Use `iter_mut` to return mutable references
}
