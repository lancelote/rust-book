use std::collections::HashMap;
use std::hash::Hash;
use std::thread;
use std::time::Duration;

fn main() {
    let simulated_use_specified_value = 10;
    let simulated_random_number = 7;

    generate_workout(simulated_use_specified_value, simulated_random_number);
}

fn generate_workout(intensity: u32, random_number: u32) {
    let mut expensive_result = Cache::new(|num| {
        println!("calculating slowly...");
        thread::sleep(Duration::from_secs(2));
        num
    });

    if intensity < 25 {
        println!("Today, do {} pushups!", expensive_result.value(&intensity));
        println!("Next, do {} situps!", expensive_result.value(&intensity));
    } else if random_number == 3 {
        println!("Take a break today! Remember to stay hydrated!");
    } else {
        println!(
            "Today, run for {} minutes!",
            expensive_result.value(&intensity)
        );
    }
}

struct Cache<'k, F, K, V>
where
    F: Fn(&'k K) -> V,
    K: Hash,
{
    calculation: F,
    values: HashMap<&'k K, V>,
}

impl<'k, F, K, V> Cache<'k, F, K, V>
where
    F: Fn(&'k K) -> V,
    K: Eq + Hash,
    V: Clone,
{
    fn new(calculation: F) -> Cache<'k, F, K, V> {
        Cache {
            calculation,
            values: HashMap::new(),
        }
    }

    fn value(&mut self, arg: &'k K) -> V {
        let calculate = &self.calculation;
        self.values
            .entry(arg)
            .or_insert_with(|| calculate(arg))
            .clone()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn call_with_different_values() {
        let mut cache = Cache::new(|&a| a);

        let v1 = cache.value(&1);
        let v2 = cache.value(&2);

        assert_eq!(v1, 1);
        assert_eq!(v2, 2);
    }

    #[test]
    fn can_call_on_other_types() {
        let mut cache = Cache::new(|&a| a);

        let v1 = cache.value(&"foo");
        let v2 = cache.value(&"bar");

        assert_eq!(v1, "foo");
        assert_eq!(v2, "bar");
    }

    #[test]
    fn can_return_different_type() {
        let mut cache = Cache::new(|&a: &u32| a.to_string());

        let v1 = cache.value(&1);
        let v2 = cache.value(&2);

        assert_eq!(v1, "1");
        assert_eq!(v2, "2");
    }
}
