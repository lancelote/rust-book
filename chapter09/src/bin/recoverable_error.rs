use std::fs::File;

fn main() {
    let _f = match File::open("sample.txt") {
        Ok(file) => file,
        Err(error) => panic!("can't read file: {:?}", error),
    };
}
