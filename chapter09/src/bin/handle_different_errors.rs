use std::fs::File;
use std::io::ErrorKind;

fn main() {
    let _f = match File::open("hello.txt") {
        Ok(file) => file,
        Err(error) => match error.kind() {
            ErrorKind::NotFound => match File::create("hello.txt") {
                Ok(fc) => fc,
                Err(error) => panic!("can't create file: {:?}", error),
            },
            other_error => panic!("can't open file: {:?}", other_error),
        },
    };
}
