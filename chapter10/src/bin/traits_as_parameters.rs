use std::fmt::Display;

pub trait Summary {
    fn summarize(&self) -> String {
        String::from("(Read more ...)")
    }
}

pub struct NewsArticle {
    pub headline: String,
    pub location: String,
    pub author: String,
    pub content: String,
}

impl Summary for NewsArticle {}

pub fn notify(item: impl Summary) {
    println!("Breaking news! {}", item.summarize());
}

pub fn notify_many<T: Summary>(item1: T, item2: T) {
    println!("{} and {}", item1.summarize(), item2.summarize())
}

pub fn notify_with_display(item: impl Summary + Display) {
    println!("{}", item.summarize());
}

pub fn notify_many_with_display<T: Summary + Display>(item1: T, item2: T) {
    println!("{} and {}", item1.summarize(), item2.summarize());
}

fn main() {}
