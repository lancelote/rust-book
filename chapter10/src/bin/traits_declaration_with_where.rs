use std::fmt::{Debug, Display};

fn some_function<T, U>(t: T, u: U) -> i32
where
    T: Display + Clone,
    U: Clone + Debug,
{
    println!("{}, {:?}", t, u);
    42
}

fn main() {
    some_function(1, 2);
}
