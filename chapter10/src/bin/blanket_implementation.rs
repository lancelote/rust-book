use std::fmt::Display;

pub trait ToMessage {
    fn to_message(&self) -> String;
}

impl<T: Display> ToMessage for T {
    fn to_message(&self) -> String {
        format!("message: {}", self)
    }
}

fn print<T: ToMessage>(message: T) {
    println!("{}", message.to_message());
}

fn main() {
    print(1);
}
