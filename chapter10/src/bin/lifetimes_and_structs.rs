// Instance of ImportantExcerpt can't outlive part reference
struct ImportantExcerpt<'a> {
    part: &'a str,
}

fn main() {
    let novel = String::from("Call me Ismael. Some years ago ...");
    let first_sentence: &str = novel.split('.').next().expect("Could not find '.'");
    let excerpt = ImportantExcerpt {
        part: first_sentence,
    };
    println!("The part is {}", excerpt.part);
}
