use std::fmt::Display;

fn longest_with_an_announcement<'a, T>(x: &'a str, y: &'a str, message: T) -> &'a str
where
    T: Display,
{
    println!("Announcement: {}", message);
    if x.len() > y.len() {
        x
    } else {
        y
    }
}

fn main() {
    let first = "hello";
    let second = "world";

    let result = longest_with_an_announcement(first, second, "Hi!");
    println!("Result: {}", result);
}
