const PRESENTS: [&str; 12] = [
    "A Partridge in a Pear Tree",
    "Two Turtle Doves",
    "Three French Hens",
    "Four Calling Birds",
    "Five Golden Rings",
    "Six Geese a Laying",
    "Seven Swans a Swimming",
    "Eight Maids a Milking",
    "Nine Ladies Dancing",
    "Ten Lords a Leaping",
    "Eleven Pipers Piping",
    "12 Drummers Drumming",
];

const DAYS: [&str; 12] = [
    "first", "second", "third", "fourth", "fifth", "sixth", "seventh", "eighth", "ninth", "tenth",
    "eleventh", "twelfth",
];

fn main() {
    for (n, day) in DAYS.iter().enumerate() {
        println!("On the {} day of Christmas", day);
        println!("my true love sent to me:");

        for i in (1..=n).rev() {
            println!("{}", PRESENTS[i]);
        }

        match n {
            0 => println!("A Partridge in a Pear Tree"),
            _ => println!("And a Partridge in a Pear Tree"),
        }

        println!();
    }
}
