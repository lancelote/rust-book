use std::io;

fn convert_fahrenheit(fahrenheit: i32) {
    println!("  = {} Celsius", (fahrenheit - 32) * 5 / 9);
}

fn convert_celsius(celsius: i32) {
    println!("  = {} Fahrenheit", celsius * 9 / 5 + 32);
}

fn main() {
    let mut input = String::new();

    println!("Please select the converter:");
    println!("1. F -> C");
    println!("2. C -> F");

    io::stdin()
        .read_line(&mut input)
        .expect("Failed to read STDIN");

    let convert = match input.trim().parse().expect("Wrong input") {
        1 => {
            println!("Enter Fahrenheit:");
            convert_fahrenheit
        }
        2 => {
            println!("Enter Celsius:");
            convert_celsius
        }
        _ => {
            println!("Unknown converter");
            return;
        }
    };

    input.clear();
    io::stdin()
        .read_line(&mut input)
        .expect("Failed to read STDIN");

    let grad: i32 = match input.trim().parse() {
        Ok(num) => num,
        Err(_) => {
            println!("Wrong temperature format");
            return;
        }
    };
    convert(grad);
}
