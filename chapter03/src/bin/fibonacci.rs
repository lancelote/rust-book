use std::io;

fn main() {
    let mut input = String::new();
    let mut x0 = 0;
    let mut x1 = 1;

    println!("Enter Fibonacci number index:");

    io::stdin()
        .read_line(&mut input)
        .expect("Failed to read line");

    let mut index: u32 = match input.trim().parse() {
        Ok(num) => num,
        Err(_) => {
            println!("Wrong input type");
            return;
        }
    };

    let result: u32 = match index {
        1 => 0,
        2 => 1,
        _ => {
            while index != 2 {
                let tmp = x0 + x1;
                x0 = x1;
                x1 = tmp;
                index -= 1;
            }
            x1
        }
    };

    println!("Number: {}", result);
}
