#[derive(Debug)]
enum UsState {
    Alabama,
    Alaska,
}

enum Coin {
    Penny,
    Nickel,
    Dime,
    Quarter(UsState),
}

fn value_in_cents(coin: Coin) -> u8 {
    match coin {
        Coin::Penny => 1,
        Coin::Nickel => 5,
        Coin::Dime => 10,
        Coin::Quarter(state) => {
            println!("State quarter from {:?}!", state);
            25
        }
    }
}

fn main() {
    let penny = Coin::Penny;
    let nickel = Coin::Nickel;
    let dime = Coin::Dime;
    let alabama_quarter = Coin::Quarter(UsState::Alabama);
    let alaska_quarter = Coin::Quarter(UsState::Alaska);

    println!("penny is {} cents", value_in_cents(penny));
    println!("nickel is {} cents", value_in_cents(nickel));
    println!("dime is {} cents", value_in_cents(dime));
    println!(
        "alabama quarter is {} cents",
        value_in_cents(alabama_quarter)
    );
    println!("alaska quarter is {} cents", value_in_cents(alaska_quarter));
}
