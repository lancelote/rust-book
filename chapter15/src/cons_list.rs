#[derive(Debug)]
pub enum List {
    Cons(i32, Box<List>),
    Nil,
}

pub use List::{Cons, Nil};

#[test]
fn main() {
    let list = Cons(1, Box::new(Cons(2, Box::new(Cons(3, Box::new(Nil))))));

    println!("{:?}", list);
}
