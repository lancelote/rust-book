use std::rc::Rc;

pub enum List {
    Cons(i32, Rc<List>),
    Nil,
}

#[cfg(test)]
mod tests {
    use super::List::{Cons, Nil};
    use std::rc::Rc;

    #[test]
    fn main() {
        let a = Rc::new(Cons(5, Rc::new(Cons(10, Rc::new(Nil)))));
        let _b = Cons(3, Rc::clone(&a));
        let _c = Cons(4, Rc::clone(&a));
    }
}
