pub use crate::drop_trait::CustomSmartPointer;

#[test]
fn main() {
    let c = CustomSmartPointer {
        data: String::from("some data"),
    };
    println!("CustomSmartPointer is created");
    drop(c);
    println!("CustomSmartPointer is dropped before the end of the main");
}
