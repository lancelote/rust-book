pub fn hello(name: &str) {
    println!("Hello, {}!", name);
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::our_own_smart_pointer::MyBox;

    #[test]
    fn implicit_deref() {
        let m = MyBox::new(String::from("Rust"));
        hello(&m)
    }

    #[test]
    fn explicit_deref() {
        let m = MyBox::new(String::from("Rust"));
        hello(&(*m)[..])
    }
}
