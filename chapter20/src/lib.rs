use std::sync::{mpsc, Arc, Mutex};
use std::thread;

trait FnBox {
    fn call_box(self: Box<Self>);
}

impl<F: FnOnce()> FnBox for F {
    fn call_box(self: Box<F>) {
        (*self)()
    }
}

type Job = Box<dyn FnBox + Send + 'static>;

enum Message {
    NewJob(Job),
    Terminate,
}

struct Worker {
    id: usize,
    thread: Option<thread::JoinHandle<()>>,
}

impl Worker {
    fn new(id: usize, receiver: Arc<Mutex<mpsc::Receiver<Message>>>) -> Worker {
        let thread = thread::spawn(move || {
            // Using loop instead of while to trigger the unlock
            loop {
                // lock to acquire the mutex
                let message = receiver.lock().unwrap().recv().unwrap();
                // MutexGuard is dropped here

                match message {
                    Message::NewJob(job) => {
                        println!("worker {} got a job, executing ...", id);
                        job.call_box();
                    }
                    Message::Terminate => {
                        println!("worker {} got a termination signal, stopping ...", id);
                        break;
                    }
                }
            }
        });

        Worker {
            id,
            thread: Some(thread),
        }
    }
}

pub struct ThreadPool {
    workers: Vec<Worker>,
    sender: mpsc::Sender<Message>,
}

impl ThreadPool {
    /// Create a new ThreadPool
    ///
    /// The size is the number of threads in the pool.
    ///
    /// # Panics
    ///
    /// The `new` function will panic if the size is zero.
    pub fn new(size: usize) -> ThreadPool {
        assert!(size > 0);

        let (sender, receiver) = mpsc::channel();

        // Arc - allow multiple workers to own receiver
        // Mutex - ensure only one worker gets a job from receiver
        let receiver = Arc::new(Mutex::new(receiver));

        // `with_capacity` pre-allocates the space
        let mut workers = Vec::with_capacity(size);

        for id in 0..size {
            // Use Arc::clone to bump the reference counter
            workers.push(Worker::new(id, Arc::clone(&receiver)));
        }

        ThreadPool { workers, sender }
    }

    pub fn execute<F>(&self, f: F)
    where
        F: FnOnce() + Send + 'static,
    {
        let job = Box::new(f);

        self.sender.send(Message::NewJob(job)).unwrap();
    }
}

impl Drop for ThreadPool {
    fn drop(&mut self) {
        println!("sending termination signal to all workers ...");

        for _ in &mut self.workers {
            self.sender.send(Message::Terminate).unwrap();
        }

        println!("shutting down all workers ...");

        for worker in &mut self.workers {
            println!("shutting down worker {} ...", worker.id);

            if let Some(thread) = worker.thread.take() {
                thread.join().unwrap();
            }
        }
    }
}
