pub fn greeting(name: &str) -> String {
    format!("Hello {}!", name)
}

fn main() {
    println!("{}", greeting("Pavel"));
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn greeting_contains_name() {
        let result = greeting("Carol");
        assert!(
            result.contains("Carol"),
            "Greeting did not contain name, value was: `{}`",
            result
        );
    }
}
