pub struct Guess {
    value: i32,
}

impl Guess {
    pub fn new(value: i32) -> Guess {
        if value < 1 {
            panic!("Guess value must be >= 1, got {}", value);
        } else if value > 100 {
            panic!("Guess value must be <= 100, got {}", value);
        }

        Guess { value }
    }
}

fn main() {
    let guess = Guess { value: 42 };
    println!("Guess is {}", guess.value);
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    #[should_panic(expected = "Guess value must be >= 1, got 0")]
    fn greater_than_100() {
        Guess::new(0);
    }
}
