fn main() {
    println!("Hello tests!");
}

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() -> Result<(), String> {
        let a = 2;
        let b = 2;

        if a + b == 4 {
            Ok(())
        } else {
            Err(String::from("2 + 2 != 4"))
        }
    }
}
