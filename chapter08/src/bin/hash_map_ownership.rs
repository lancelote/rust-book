use std::collections::HashMap;

fn main() {
    let field_name = String::from("favorite color");
    let field_value = String::from("blue");

    let mut map = HashMap::new();
    map.insert(field_name, field_value);

    println!("{:?}", map);
    // field_name and field_value are no longer available here
}
