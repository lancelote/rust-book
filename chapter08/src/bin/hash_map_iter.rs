use std::collections::HashMap;

fn main() {
    let mut scores = HashMap::new();

    scores.insert(String::from("blue"), 10);
    scores.insert(String::from("red"), 50);

    for (key, value) in &scores {
        println!("{}: {}", key, value);
    }
}
