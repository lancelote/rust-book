use std::io;

// Convert strings to pig latin. The first consonant of each word is moved to the end of the word
// and “ay” is added, so “first” becomes “irst-fay.” Words that start with a vowel have “hay”
// added to the end instead (“apple” becomes “apple-hay”).
fn main() {
    let mut input = String::new();
    let mut latin_phrase = String::new();

    io::stdin()
        .read_line(&mut input)
        .expect("failed to read input");

    for word in input.split_whitespace() {
        let mut chars = word.chars();

        let latin_word = match chars.next() {
            Some(first) => match first {
                'a' | 'e' | 'i' | 'o' | 'u' => format!("{}-hay", word),
                first => format!("{}-{}ay", chars.as_str(), first),
            },
            None => String::new(),
        };

        latin_phrase.push_str(&latin_word);
        latin_phrase.push_str(" ");
    }

    println!("{}", latin_phrase)
}
