use std::collections::HashMap;
use std::io;

fn mean(numbers: &[i32]) -> f32 {
    numbers.iter().sum::<i32>() as f32 / numbers.len() as f32
}

fn median(numbers: &mut Vec<i32>) -> i32 {
    numbers.sort();
    numbers[numbers.len() / 2]
}

fn mode(numbers: &[i32]) -> i32 {
    let mut map = HashMap::new();

    for &number in numbers {
        *map.entry(number).or_insert(0) += 1;
    }

    map.into_iter()
        .max_by_key(|&(_, count)| count)
        .map(|(val, _)| val)
        .expect("can't compute mode for empty sequence")
}

fn main() {
    let mut input = String::new();
    let mut nums = Vec::new();
    let mut map = HashMap::new();

    io::stdin()
        .read_line(&mut input)
        .expect("failed to read input");

    for element in input.split_whitespace() {
        let num: i32 = element.trim().parse().expect("failed to parse element");
        let count = map.entry(num).or_insert(0);
        *count += 1;
        nums.push(num);
    }

    println!("mean: {}", mean(&nums));
    println!("median: {}", median(&mut nums));
    println!("mode: {}", mode(&nums));
}
