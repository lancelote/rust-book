use std::collections::HashMap;

fn main() {
    let mut scores = HashMap::new();
    scores.insert(String::from("blue"), 10);

    scores.entry(String::from("red")).or_insert(50);
    scores.entry(String::from("blue")).or_insert(50);

    println!("{:?}", scores);
}
