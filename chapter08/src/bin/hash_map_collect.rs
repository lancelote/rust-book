use std::collections::HashMap;

fn main() {
    let teams = vec![String::from("blue"), String::from("red")];
    let initial_scores = vec![10, 50];
    let scores: HashMap<_, _> = teams.iter().zip(initial_scores.iter()).collect();

    println!("{:?}", scores);
}
