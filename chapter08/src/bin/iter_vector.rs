fn main() {
    let v1 = vec![100, 32, 57];

    for i in &v1 {
        println!("{}", i);
    }

    let mut v2 = vec![100, 32, 57];
    println!("old vector values: {:?}", v2);

    for i in &mut v2 {
        *i += 50;
    }
    println!("new vector values: {:?}", v2);
}
