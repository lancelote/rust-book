use std::collections::HashMap;

fn main() {
    let mut scores = HashMap::new();

    scores.insert(String::from("blue"), 10);
    scores.insert(String::from("red"), 50);

    let team_name = String::from("blue");
    let score = scores.get(&team_name);
    println!("team: {}, score: {:?}", team_name, score);
}
