// Using a hash map and vectors, create a text interface to allow a user to add employee names
// to a department in a company. For example, “Add Sally to Engineering” or “Add Amir to Sales.”
// Then let the user retrieve a list of all people in a department or all people in the company
// by department, sorted alphabetically.

use std::collections::HashMap;
use std::io;

enum Command {
    Add { dept: String, name: String },
    List(String),
    All,
    Quit,
}

impl Command {
    fn from_line(line: &str) -> Option<Self> {
        let words: Vec<&str> = line.trim().split_whitespace().collect();
        match words.as_slice() {
            ["all"] => Some(Command::All),
            ["quit"] => Some(Command::Quit),
            ["list", dept] => Some(Command::List(dept.to_string())),
            ["add", name, "to", dept] => Some(Command::Add {
                dept: dept.to_string(),
                name: name.to_string(),
            }),
            _ => None,
        }
    }
}

fn main() {
    let mut employees: HashMap<String, Vec<String>> = HashMap::new();

    println!("all | quit | list <dept> | add <name> to <dept>");

    loop {
        let mut input = String::new();

        io::stdin().read_line(&mut input).expect("can't read STDIN");

        match Command::from_line(&input) {
            Some(Command::Add { dept, name }) => employees.entry(dept).or_default().push(name),
            Some(Command::List(dept)) => match employees.get(&dept) {
                Some(names) => {
                    for name in names {
                        println!("{}: {}", dept, name);
                    }
                }
                None => println!("unknown department"),
            },
            Some(Command::All) => {
                for (dept, names) in &employees {
                    let mut names = names.clone();
                    names.sort();
                    for name in names {
                        println!("{}: {}", dept, name);
                    }
                }
            }
            Some(Command::Quit) => break,
            None => println!("unknown command"),
        }
    }
}
