fn with_ownership() {
    let s1 = String::from("Hello, ");
    let s2 = String::from("world!");

    let s = s1 + &s2; // s1 is no longer usable
    println!("{}", s);
}

fn without_ownership() {
    let s1 = String::from("tic");
    let s2 = String::from("tac");
    let s3 = String::from("toe");

    let s = format!("{}-{}-{}", s1, s2, s3);
    println!("{}", s);
}

fn main() {
    with_ownership();
    without_ownership();
}
