pub struct Post {
    content: String,
}

pub struct DraftPost {
    content: String,
}

impl Post {
    #[allow(clippy::new_ret_no_self)]
    pub fn new() -> DraftPost {
        DraftPost {
            content: String::new(),
        }
    }

    pub fn content(&self) -> &str {
        &self.content
    }
}

impl DraftPost {
    pub fn add_text(&mut self, text: &str) {
        self.content.push_str(text);
    }

    pub fn request_review(self) -> PendingReviewRequest {
        PendingReviewRequest {
            content: self.content,
        }
    }
}

pub struct PendingReviewRequest {
    content: String,
}

impl PendingReviewRequest {
    pub fn approve(self) -> PendingFinalApprove {
        PendingFinalApprove {
            content: self.content,
        }
    }

    pub fn reject(self) -> DraftPost {
        DraftPost {
            content: self.content,
        }
    }
}

pub struct PendingFinalApprove {
    content: String,
}

impl PendingFinalApprove {
    pub fn approve(self) -> Post {
        Post {
            content: self.content,
        }
    }

    pub fn reject(self) -> DraftPost {
        DraftPost {
            content: self.content,
        }
    }
}

fn main() {
    let mut post = Post::new();

    post.add_text("I ate a salad for lunch today");

    let post = post.request_review();

    let post = post.reject();

    let post = post.request_review();

    let post = post.approve();

    let post = post.approve();

    assert_eq!("I ate a salad for lunch today", post.content());
}
