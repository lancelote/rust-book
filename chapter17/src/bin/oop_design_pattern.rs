#[derive(Default)]
pub struct Post {
    state: Option<Box<dyn State>>,
    content: String,
}

impl Post {
    pub fn new() -> Post {
        Post {
            state: Some(Box::new(Draft {})),
            content: String::new(),
        }
    }

    pub fn add_text(&mut self, text: &str) {
        if self.state.as_ref().unwrap().allow_edit() {
            self.content.push_str(text);
        }
    }

    pub fn content(&self) -> &str {
        self.state.as_ref().unwrap().content(&self)
    }

    pub fn request_review(&mut self) {
        // Takes `Some` ouf of `state` leaving `None` behind
        if let Some(s) = self.state.take() {
            self.state = Some(s.request_review())
        }
    }

    pub fn approve(&mut self) {
        if let Some(s) = self.state.take() {
            self.state = Some(s.approve())
        }
    }

    pub fn reject(&mut self) {
        if let Some(s) = self.state.take() {
            self.state = Some(s.reject())
        }
    }
}

trait State {
    fn allow_edit(&self) -> bool {
        false
    }

    // The method is only valid when called on a `Box` holding the type
    fn request_review(self: Box<Self>) -> Box<dyn State>;

    fn approve(self: Box<Self>) -> Box<dyn State>;

    fn reject(self: Box<Self>) -> Box<dyn State>;

    #[allow(unused)]
    fn content<'a>(&self, post: &'a Post) -> &'a str {
        ""
    }
}

struct Draft {}

impl State for Draft {
    fn allow_edit(&self) -> bool {
        true
    }

    fn request_review(self: Box<Self>) -> Box<dyn State> {
        Box::new(PendingReview { n: 0 })
    }

    fn approve(self: Box<Self>) -> Box<dyn State> {
        self
    }

    fn reject(self: Box<Self>) -> Box<dyn State> {
        self
    }
}

struct PendingReview {
    n: u32,
}

impl State for PendingReview {
    fn request_review(self: Box<Self>) -> Box<dyn State> {
        self
    }

    fn approve(mut self: Box<Self>) -> Box<dyn State> {
        if self.n < 1 {
            self.n += 1;
            self
        } else {
            Box::new(Published {})
        }
    }

    fn reject(self: Box<Self>) -> Box<dyn State> {
        Box::new(Draft {})
    }
}

struct Published {}

impl State for Published {
    fn request_review(self: Box<Self>) -> Box<dyn State> {
        self
    }

    fn approve(self: Box<Self>) -> Box<dyn State> {
        self
    }

    fn reject(self: Box<Self>) -> Box<dyn State> {
        self
    }

    fn content<'a>(&self, post: &'a Post) -> &'a str {
        &post.content
    }
}

fn main() {
    let mut post = Post::new();

    post.add_text("I ate a salad for lunch");
    assert_eq!("", post.content());

    post.request_review();
    assert_eq!("", post.content());

    post.add_text(" today");
    assert_eq!("I ate a salad for lunch", post.content);

    post.reject();
    assert_eq!("", post.content());

    post.add_text(" today");
    assert_eq!("I ate a salad for lunch today", post.content);

    post.request_review();
    assert_eq!("", post.content());

    post.add_text(" and yesterday");
    assert_eq!("I ate a salad for lunch today", post.content);

    // two approves are required
    post.approve();
    assert_eq!("", post.content());

    post.add_text(" and yesterday");
    assert_eq!("I ate a salad for lunch today", post.content);

    post.approve();
    assert_eq!("I ate a salad for lunch today", post.content());

    post.add_text(" and yesterday");
    assert_eq!("I ate a salad for lunch today", post.content);
}
