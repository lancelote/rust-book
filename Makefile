help:
	@echo "check"
	@echo "    run cargo check, fmt --check and clippy"
	@echo "fmt"
	@echo "    run cargo fmt for all sources"
	@echo "test"
	@echo "    run cargo test"
	@echo "git"
	@echo "    setup git hooks"

check:
	cargo check
	cargo fmt --all -- --check
	cargo clippy --all-targets --all-features -- -D warnings

fmt:
	cargo fmt --all

test:
	cargo test --verbose

git:
	git config --local core.hooksPath .githooks