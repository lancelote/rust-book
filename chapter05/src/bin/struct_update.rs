struct User {
    username: String,
    email: String,
    sign_in_count: u64,
    active: bool,
}

fn print_user(user: &User) {
    println!("email: {}", user.email);
    println!("username: {}", user.username);
    println!("active: {}", user.active);
    println!("sing in count: {}", user.sign_in_count);
    println!();
}

fn main() {
    let user1 = User {
        email: String::from("someone@example.com"),
        username: String::from("someone"),
        active: true,
        sign_in_count: 1,
    };
    let user2 = User {
        email: String::from("someone_else@example.com"),
        username: String::from("someone_else"),
        ..user1
    };

    print_user(&user1);
    print_user(&user2);
}
