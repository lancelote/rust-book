struct Color(i32, i32, i32);
struct Point(i32, i32);

fn print_color(color: &Color) {
    println!("Color({}, {}, {})", color.0, color.1, color.2);
}

fn print_point(point: &Point) {
    println!("Point({}, {})", point.0, point.1);
}

fn main() {
    let black = Color(0, 0, 0);
    let origin = Point(0, 0);

    print_color(&black);
    print_point(&origin);
}
