fn area(dimensions: (u32, u32)) -> u32 {
    dimensions.0 * dimensions.1
}

fn main() {
    let rectangle = (30, 50);

    println!("The area of the rectangle is {} pixels", area(rectangle));
}
