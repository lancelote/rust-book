fn area(width: u32, height: u32) -> u32 {
    width * height
}

fn main() {
    let width = 30;
    let height = 50;

    println!(
        "The area of the rectangle is {} pixels",
        area(width, height)
    );
}
