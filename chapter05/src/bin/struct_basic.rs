struct User {
    username: String,
    email: String,
    sign_in_count: u64,
    active: bool,
}

fn build_user(email: String, username: String) -> User {
    User {
        email,
        username,
        active: true,
        sign_in_count: 1,
    }
}

fn main() {
    let email = String::from("someone@example.com");
    let username = String::from("someone");
    let mut user1 = build_user(email, username);

    println!("User email: {}", user1.email);
    println!("User login: {}", user1.username);
    println!("Active status: {}", user1.active);
    println!("Sign in count: {}", user1.sign_in_count);

    user1.email = String::from("someone_else@example.com");

    println!("New user email: {}", user1.email);
}
