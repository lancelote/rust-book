struct Point {
    x: i32,
    y: i32,
}

fn main() {
    let p = Point { x: 0, y: 7 };

    let Point { x: a, y: b } = p;

    assert_eq!(a, 0);
    assert_eq!(b, 7);

    // and with the same names
    let Point { x, y } = p;

    assert_eq!(x, 0);
    assert_eq!(y, 7);

    // and with match
    match p {
        Point { x, y: 0 } => println!("on the x axis at {}", x),
        Point { x: 0, y } => println!("on the y axis at {}", y),
        Point { x, y } => println!("on neither axis: ({}, {})", x, y),
    }
}
