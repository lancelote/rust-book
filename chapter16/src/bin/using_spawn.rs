use std::thread::{sleep, spawn};
use std::time::Duration;

fn main() {
    spawn(|| {
        for i in 1..10 {
            println!("hi number {} from the spawned thread!", i);
            sleep(Duration::from_millis(1));
        }
    });

    for i in 1..5 {
        println!("hi number {} from the main thread!", i);
        sleep(Duration::from_millis(1));
    }

    // Execution will finish once the main thread is done
}
