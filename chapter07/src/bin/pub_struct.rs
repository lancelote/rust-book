mod back_of_house {
    #[derive(Debug)]
    pub enum Appetizer {
        Soup,
        Salad,
    }

    pub struct Breakfast {
        pub toast: String,
        season_fruit: String,
    }

    impl Breakfast {
        pub fn summer(toast: &str) -> Breakfast {
            let breakfast = Breakfast {
                toast: String::from(toast),
                season_fruit: String::from("peaches"),
            };
            println!("Season fruit is {}", breakfast.season_fruit);
            breakfast
        }
    }
}

pub fn eat_at_restaurant() {
    let mut meal = back_of_house::Breakfast::summer("Rye");
    meal.toast = String::from("Wheat"); // change the mind
    println!("I'd like {} toast please", meal.toast);

    let order1 = back_of_house::Appetizer::Soup;
    let order2 = back_of_house::Appetizer::Salad;

    println!("Order 1 is {:?}", order1);
    println!("Order 2 is {:?}", order2);
}

fn main() {
    eat_at_restaurant();
}
