mod front_of_house {
    pub mod hosting {
        pub fn add_to_wait_list() {
            println!("adding to wait list ...");
        }
    }
}

pub fn eat_at_restaurant() {
    // absolute path
    crate::front_of_house::hosting::add_to_wait_list();

    // relative path
    front_of_house::hosting::add_to_wait_list();
}

fn main() {
    eat_at_restaurant();
}
