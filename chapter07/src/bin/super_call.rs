fn serve_order() {
    println!("serving the order ...");
}

mod back_of_house {
    pub fn fix_incorrect_order() {
        println!("fixing the incorrect order ...");
        cook_order();
        super::serve_order()
    }

    fn cook_order() {
        println!("cooking the order ...")
    }
}

fn main() {
    back_of_house::fix_incorrect_order();
}
