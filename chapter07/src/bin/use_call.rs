mod front_of_house {
    pub mod hosting {
        pub fn add_to_white_list() {
            println!("adding to whitelist ...");
        }
    }
}

use self::front_of_house::hosting;

pub fn eat_at_restaurant() {
    hosting::add_to_white_list();
}

fn main() {
    eat_at_restaurant();
}
